import cv2
import numpy as np
import RPi.GPIO as GPIO

# Inisialisasi pin Raspberry Pi untuk LED
led_pin = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(led_pin, GPIO.OUT)

# Inisialisasi kamera Raspberry Pi
camera = cv2.VideoCapture(0)
camera.set(3, 640)  # Lebar frame
camera.set(4, 480)  # Tinggi frame

# Rentang warna oranye dalam HSV
orange_lower = np.array([5, 50, 50], dtype=np.uint8)
orange_upper = np.array([15, 255, 255], dtype=np.uint8)

try:
    while True:
        # Baca frame dari kamera
        ret, frame = camera.read()

        # Konversi frame menjadi ruang warna HSV
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # Deteksi objek oranye
        mask = cv2.inRange(hsv, orange_lower, orange_upper)

        # Dilasi dan penapisan citra untuk menghilangkan noise
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)

        # Mencari kontur objek
        contours, _ = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # Mencari objek berwarna oranye
        orange_detected = False
        for contour in contours:
            area = cv2.contourArea(contour)
            if area > 1000:
                x, y, w, h = cv2.boundingRect(contour)
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                cv2.putText(frame, '+', (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2)
                orange_detected = True

                # Tentukan posisi objek oranye
                frame_width = frame.shape[1]
                obj_center_x = x + w // 2
                if obj_center_x < frame_width // 3:
                    cv2.putText(frame, 'Posisi: Kiri', (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2)
                elif obj_center_x > frame_width * 2 // 3:
                    cv2.putText(frame, 'Posisi: Kanan', (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2)
                else:
                    cv2.putText(frame, 'Posisi: Tengah', (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2)
        
        # Menghidupkan atau mematikan LED berdasarkan deteksi objek oranye
        if orange_detected:
            GPIO.output(led_pin, GPIO.HIGH)
        else:
            GPIO.output(led_pin, GPIO.LOW)

        # Tampilkan frame hasil
        cv2.imshow("tes", frame)

        # Tombol 'q' untuk keluar
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

finally:
    # Bersihkan GPIO dan tutup jendela tampilan
    GPIO.cleanup()
    cv2.destroyAllWindows()
    camera.release()
